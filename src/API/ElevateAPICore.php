<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
abstract class ElevateAPICore
{
    protected $password;

    protected $username;

    protected $brand;

    protected $wsdl_uri;

    protected $client = null;

    protected $token = null;

    public function __construct()
    {
        $this->brand = env('ELEVATE_BRAND');

        $this->username = env('ELEVATE_USERNAME');

        $this->password = env('ELEVATE_PASSWORD');

        $this->billing_client_id = env('ELEVATE_BILLING_CLIENT_ID');

        $this->password = env('ELEVATE_PASSWORD');

        $this->wsdl_server = env('ELEVATE_SERVER');

        $this->wsdl_uri = $this->getWSDLUri('securityFacade');

        $this->security_token = null;
    }

    public function __call($method, $arguments)
    {
        if (method_exists($this, $method)) {
            $this->getToken();

            return call_user_func_array(array($this, $method), $arguments);
        }

        throw new \Exception('Method: ' . $method . ' does not exist!');
    }

    /**
     * Sets the client if not already set, this should be called by magic call method on each method request
     */
    public function setClient()
    {
        if (!isset($this->client)) {
            $this->client = new \SoapClient($this->wsdl_uri, array('trace' => true, 'exceptions' => true));

            $this->client->__setLocation($this->wsdl_uri);
        }
    }

    public function getClient()
    {
        return $this->client;
    }

    /**
     * Returns the string token provided by affinity
     * @return null
     */
    public function getToken()
    {
        $this->setClient();

        $data = [
            'username' => $this->username,
            'secrets' => $this->password,
            'brandName' => $this->brand,
            'defaultTokenExpirationInMins' => 2
        ];

        $xml = view('elevate-api::xml.security.create-security-token')->with($data)->render();

        $result = $this->makeAPICall('securityFacade', 'createSecurityToken', $xml);

        if(isset($result->userAndToken->securityToken)){
            $this->security_token = $result->userAndToken->securityToken;

            return $result;
        }else{
            throw new \Exception('Login to Elevate API failed');
        }

    }

    /**
     * Turns an XML string response into an object
     * @param $xml_string
     * @return object
     */
    protected function xmlToObject($xml_string)
    {
        $xml_string = str_replace('<?xml version="1.0" ?>', '', $xml_string);

        $xml_string = str_replace('S:', '', $xml_string);

        $xml_string = str_replace('ns2:', '', $xml_string);

        $xml_object = simplexml_load_string($xml_string);

        $xml_object = json_decode(json_encode($xml_object));

        $top_attribute_name = array_keys((array)$xml_object->Body);

        $top_attribute_name = $top_attribute_name[0];

        if($top_attribute_name == 'Fault') return $xml_object->Body->$top_attribute_name;

        return $xml_object->Body->$top_attribute_name->return;
    }

    /**
     * Converts an associate array into an XML string
     * @param array $array
     * @return string
     */
    protected function arrayToXMLString(array $array, $root = null)
    {
        $xml_string = '';

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $child_string = $this->arrayToXMLString($value);

                $xml_string .= '<' . $key . '>' . $child_string . '</' . $key . '>';
            } elseif (is_object($value)) {
                $xml_string .= '<' . $key . '></' . $key . '>';
            } else {
                $xml_string .= '<' . $key . '>' . $value . '</' . $key . '>';
            }
        }

        return ($root == null) ? $xml_string : '<' . $root . '>' . $xml_string . '</' . $root . '>';
    }

    /**
     * Determines if the response from Affinity was an error
     * @param $response
     * @return bool
     */
    public function errorResponse($response)
    {
        if (!isset($response->ResponseCode) || $response->ResponseCode != 0) return true;
    }


    /**
     * This is used to keep error report standard across the Affinity API
     * @param \ $response
     * @return object
     */
    protected function reportError($response)
    {
        $response->Error = true;

        return $response;
    }

    /**
     * Gets the setting based on environment
     * @param $key
     * @return mixed
     */
    public function getSetting($key)
    {
        return $this->settings[$key];
    }

    /**
     * returns all the settings for environment
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Sets the WSDL uri
     * @param $facade
     * @return string
     */
    public function getWSDLUri($facade)
    {
        return $this->wsdl_server . $facade . '?wsdl';
    }

    /**
     * Makes the call to the Elevate API
     * @param $facade
     * @param $action
     * @param $xml
     * @return object
     */
    public function makeAPICall($facade, $action, $xml, $raw = false)
    {
        try {
            $this->wsdl_uri = $this->getWSDLUri($facade);

            $result = $this->client->__doRequest($xml, $this->wsdl_uri, $action, 1);

             //if ($facade != 'securityFacade') dd($this->wsdl_uri);
             if ($raw && $facade != 'securityFacade') dd($result);

            $result_object = $this->xmlToObject($result);

            return (isset($result_object->list)) ? $result_object->list : $result_object;
        } catch (\Exception $e) {
            \Logger::error('Elevate API Core', 'Unable to complete API call / convert with response: '.serialize($result));
        }

    }

    public function setData(array $data = [])
    {
        $token = ['token' => ['billingClientId' => $this->billing_client_id,
            'securityToken' => $this->security_token,
            'username' => $this->username,
            'defaultTokenExpirationInMins' => 2]];

        return array_merge($token, $data);
    }

    /**
     * Function returns XML string for input associative array.
     * @param array $array Input associative array
     * @param String $wrap Wrapping tag
     */
    function arrayToXml($array, $wrap = null)
    {
        $xml = '';

        if ($wrap != null) $xml .= "<$wrap>";

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $xml .= $this->arrayToXml($value, $key);
            } else {
                $xml .= "<$key>" . htmlspecialchars(trim($value)) . "</$key>";
            }
        }

        if ($wrap != null) $xml .= "</$wrap>";

        return $xml;
    }
}