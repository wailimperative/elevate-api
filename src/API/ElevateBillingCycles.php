<?php namespace Imperative\ElevateAPI\API;

use Carbon\Carbon;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateBillingCycles extends ElevateAPICore
{
    protected $facade = 'billingCycleFacade';

    /**
     * Gets array of all billing cycles
     * @return object
     */
    protected function findBillingCycles()
    {
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.billing-cycle.find-billing-cycles')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findBillingCycles', $xml);
    }

    /**
     * Gets array of all billing cycles done recently
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return object
     */
    protected function findBillingCyclesDuringPeriod(Carbon $start_date, Carbon $end_date)
    {
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.billing-cycle.find-billing-cycles-recently')->with($data)->render();
//TODO: Need to know how to do the operrators ranges in xml blade
        return $this->makeAPICall($this->facade, 'findBillingCycles', $xml)->with('start_date', $start_date)->with('end_date', $end_date);
    }
}