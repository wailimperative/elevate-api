<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateBillingRuns
 * @package Imperative\ElevateAPI\API
 */
class ElevateBillingRuns extends ElevateAPICore
{
    protected $facade = 'billingRunFacade';

    /**
     * Gets a page of billing runs
     * @param $first_result
     * @param int $max_results
     * @param int $max_download_results
     * @return object
     */
    protected function findPageOfBillingRuns($first_result, $max_results = 1000, $max_download_results = 1000)
    {
        $data = $this->setData(['first_result' => $first_result, 'max_results' => $max_results, 'max_download_results' => $max_download_results]);

        $xml = view('elevate-api::xml.billing-runs.find-page-of-billing-runs')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findPageOfBillingRuns', $xml);
    }

    /**
     * Gets a page of recent billing runs
     * @return object
     */
    protected function findPageOfBillingRunsRecent()
    {
        $data = $this->setData();

        $xml = view('elevate-api::xml.billing-runs.find-page-of-billing-runs-recent')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findPageOfBillingRuns', $xml);
    }

    /**
     * Gets page of invoices for a billing run
     * @param int $billing_run_id
     * @param $first_result
     * @param int $max_results
     * @param int $max_download_results
     * @return object
     */
    protected function findPageOfInvoicesForBillRun($billing_run_id, $first_result, $max_results = 1000, $max_download_results = 1000)
    {
        $data = $this->setData(['billing_run_id' => $billing_run_id, 'first_result' => $first_result, 'max_results' => $max_results, 'max_download_results' => $max_download_results]);

        $xml = view('elevate-api::xml.billing-runs.find-page-of-invoices-for-bill-run')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findPageOfInvoicesForBillRun', $xml);
    }
}