<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateCLIs extends ElevateAPICore
{
    protected $facade = 'RentalProductInventoryFacade';


    protected function findCLIsByCustomers($elevate_customer_id)
    {
        $data = $this->setData(['elevate_customer_id' => $elevate_customer_id, 'first_result' => 0, 'max_results' => 1000, 'max_download_results' => 1000]);

        $xml = view('elevate-api::xml.cli.find-cli-by-customer')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findCLIsByCustomer', $xml);
    }

}