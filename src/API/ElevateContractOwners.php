<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateContractOwners extends ElevateAPICore
{
    protected $facade = 'contractOwnerFacade';

    /**
     * Gets array of all contract owners
     * @return object
     */
    protected function findAllContractOwners()
    {
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.contract-owner.find-all-contract-owners')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findPageOfContractOwners', $xml);
    }
}