<?php namespace Imperative\ElevateAPI\API;

use Imperative\ElevateAPI\API\Requests\Customer\CustomerStoreRequest;


/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateCustomers extends ElevateAPICore
{
    protected $facade = 'customerFacade';

    /**
     * Creates a customer on Elevate
     * @param CustomerStoreRequest $customerStoreModel
     * @param integer $contract_owner_id
     * @return object
     */
    protected function createCustomerAndSites(CustomerStoreRequest $customerStoreModel, int $contract_owner_id)
    {
        $xml_data = $this->arrayToXml($customerStoreModel->getRequestArray());

        $data = $this->setData(['xml_data' => $xml_data, 'contract_owner_id' => $contract_owner_id]);

        $xml = view('elevate-api::xml.customer.create-customer-and-sites-with-defaults')->with($data)->render();

        return $this->makeAPICall($this->facade, 'createCustomerAndSitesWithDefaults', $xml);
    }

    /**
     * Finds a customer based on Elevate ID
     * @param $id
     * @return object
     */
    protected function findCustomerByID(int $id)
    {
        $data = $this->setData(['id' => $id]);

        $xml = view('elevate-api::xml.customer.find-customer')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findCustomer', $xml);
    }

    /**
     * Gets a page of customers
     * @param $first_result
     * @param int $max_results
     * @param int $max_download_results
     * @return object
     */
    protected function findPageOfCustomers($first_result, $max_results = 1000, $max_download_results = 1000)
    {
        $data = $this->setData(['first_result' => $first_result, 'max_results' => $max_results, 'max_download_results' => $max_download_results]);

        $xml = view('elevate-api::xml.customer.find-page-of-customers')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findPageOfCustomers', $xml);
    }

}