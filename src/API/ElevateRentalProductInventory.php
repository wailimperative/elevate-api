<?php namespace Imperative\ElevateAPI\API;

use Imperative\ElevateAPI\API\Requests\Product\CreateRentalProductInventoryRequest;

/**
 * Class ElevateRentalProductInventory
 * @package App\Packages\Elevate
 */
class ElevateRentalProductInventory extends ElevateAPICore
{
    protected $facade = 'rentalProductInventoryFacade';

    /**
     * Creates a rental product inventory
     * @param CreateRentalProductInventoryRequest $createRentalProductInventoryRequest
     * @return object
     */
    protected function createRentalProductInventory(CreateRentalProductInventoryRequest $createRentalProductInventoryRequest){
        if(!$createRentalProductInventoryRequest->isValid()) throw new \Exception('Elevate API error creating product inventory. '.serialize($createRentalProductInventoryRequest->getErrors()));

        $xml_data = $this->arrayToXml($createRentalProductInventoryRequest->getRequestArray());

        $data = $this->setData(['xml_data' => $xml_data]);

        $xml = view('elevate-api::xml.product.create-rental-product-inventory')->with($data)->render();

        return $this->makeAPICall($this->facade, 'createRentalProductInventory', $xml);
    }

    /**
     * Get all the product inventory for a site
     * @param int $elevate_customer_id
     * @return object
     */
    protected function findRentalInventorySummariesByCustomer(int $elevate_customer_id){
        $data = $this->setData(['elevate_customer_id' => $elevate_customer_id]);

        $xml = view('elevate-api::xml.product.find-rental-inventory-summaries-by-customer')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findRentalInventorySummariesByCustomer', $xml);
    }
}