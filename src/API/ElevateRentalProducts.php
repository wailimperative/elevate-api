<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateRentalProducts extends ElevateAPICore
{
    protected $facade = 'productFacade';

    /**
     * Gets array of all rental products
     * @return object
     */
    protected function findAllProductCategories()
    {
        $data = $this->setData(['arg2' => 'USAGE']);

        $xml = view('elevate-api::xml.product.find-all-product-categories')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findAllProductCategories', $xml);
    }

    /**
     * Gets array of all rental products by category
     * @return object
     */
    protected function findAllRentalProductsByCategory(int $category_id)
    {
        $data = $this->setData(['category_id' => $category_id]);

        $xml = view('elevate-api::xml.product.find-all-rental-products-by-category')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findAllRentalProductsByCategory', $xml);
    }

    /**
     * Returns all rental products
     * @return object
     */
    protected function findAllRentalProducts(){
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.product.find-all-rental-products')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findAllRentalProducts', $xml);
    }

    /**
     * Gets the rental product by ID
     * @param $id
     * @return object
     */
    protected function findRentalProduct($id){
        $data = $this->setData(['id' => $id]);

        $xml = view('elevate-api::xml.product.find-rental-product')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findRentalProduct', $xml);
    }
}