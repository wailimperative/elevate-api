<?php namespace Imperative\ElevateAPI\API;
use Imperative\ElevateAPI\API\Requests\Product\CreateRentalProductInventoryOverrideRequest;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateRentalRatingCards extends ElevateAPICore
{
    protected $facade = 'rentalRatingFacade';

    /**
     * Gets array of all rental rating cards
     * @return object
     */
    protected function findAllRentalRatingCards()
    {
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.rental-rating.find-all-rental-rating-cards')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findRentalRateCards', $xml);
    }

    /**
     * Creates a rental rating card assignment
     * @param $elevate_customer_id
     * @param elevate_rental_rating_card_id
     * @return object
     */
    protected function createRentalRatingCardAssignment($elevate_customer_id, $elevate_rental_rating_card_id, $start_date)
    {
        $data = $this->setData(['elevate_customer_id' => $elevate_customer_id, 'elevate_rental_rating_card_id' => $elevate_rental_rating_card_id, 'start_date' => $start_date]);

        $xml = view('elevate-api::xml.rental-rating.create-rental-rating-card-assignment')->with($data)->render();

        return $this->makeAPICall($this->facade, 'createRentalRateCardToCustomerAssignment', $xml);
    }


    /**
     * Creates a price override for a rental product inventory product
     * @param CreateRentalProductInventoryOverrideRequest $createRentalProductInventoryOverrideRequest
     * @return object
     */
    protected function createRentalRateOverrideToInventoryAssignment(CreateRentalProductInventoryOverrideRequest $createRentalProductInventoryOverrideRequest){
        $xml_data = $this->arrayToXml($createRentalProductInventoryOverrideRequest->getRequestArray());

        $data = $this->setData(['xml_data' => $xml_data]);

        $xml = view('elevate-api::xml.rental-rating.create-rental-rate-override-to-inventory-assignment')->with($data)->render();

        return $this->makeAPICall($this->facade, 'createRentalRateOverrideToInventoryAssignment', $xml);
    }
}