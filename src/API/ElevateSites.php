<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateSites
 * @package App\Packages\Elevate
 */
class ElevateSites extends ElevateAPICore
{
    protected $facade = 'siteFacade';

    protected function findAllSites()
    {
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.site.find-all-sites')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findAllSites', $xml);
    }

    protected function findSitesByCustomerID($id)
    {
        $data = $this->setData(['id' => $id]);

        $xml = view('elevate-api::xml.site.find-sites-by-customer-id')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findSitesByCustomerId', $xml);
    }
}