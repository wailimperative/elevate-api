<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateSuppliers extends ElevateAPICore
{
    protected $facade = 'supplierFacade';

    /**
     * Gets array of all suppliers
     * @return object
     */
    protected function findAllSuppliers()
    {
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.supplier.find-all-suppliers')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findAllSuppliers', $xml);
    }
}