<?php namespace Imperative\ElevateAPI\API;

use Imperative\ElevateAPI\API\Requests\Product\CreateUsageProductInventoryRequest;

/**
 * Class ElevateUsageProductInventory
 * @package Imperative\ElevateAPI\API
 */
class ElevateUsageProductInventory extends ElevateAPICore
{
    protected $facade = 'usageProductInventoryFacade';

    /**
     * Creates an inventory usage item
     * @param CreateUsageProductInventoryRequest $createUsageProductInventoryRequest
     * @return object
     */
    protected function createUsageProductInventory(CreateUsageProductInventoryRequest $createUsageProductInventoryRequest){
        $xml_data = $this->arrayToXml($createUsageProductInventoryRequest->getRequestArray());

        $data = $this->setData(['xml_data' => $xml_data]);

        $xml = view('elevate-api::xml.product.create-usage-product-inventory')->with($data)->render();

        return $this->makeAPICall($this->facade, 'createUsageProductInventory', $xml);
    }

    /**
     * Get all the product inventory for a site
     * @param int $elevate_site_id
     * @return object
     */
    protected function findUsageInventorySummariesByCustomer(int $elevate_site_id){
        $data = $this->setData(['elevate_site_id' => $elevate_site_id]);

        $xml = view('elevate-api::xml.product.find-usage-inventory-summaries-by-customer')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findUsageInventorySummariesByCustomer', $xml);
    }
}