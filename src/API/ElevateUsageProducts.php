<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateUsageProducts extends ElevateAPICore
{
    protected $facade = 'productFacade';

    /**
     * Gets array of all usage products
     * @return object
     */
    protected function findAllUsageProducts()
    {
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.product.find-usage-products')->with($data)->render();

        $response = $this->makeAPICall($this->facade, 'findUsageProducts', $xml);

        return $response->results;
    }
}