<?php namespace Imperative\ElevateAPI\API;

/**
 * Class ElevateAPICore
 * @package App\Packages\Elevate
 */
class ElevateUsageRatingCards extends ElevateAPICore
{
    protected $facade = 'usageRatingFacade';

    /**
     * Gets array of all usage rating cards
     * @return object
     */
    protected function findAllUsageRatingCards()
    {
        $data = $this->setData([]);

        $xml = view('elevate-api::xml.usage-rating.find-all-usage-rating-cards')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findUsageRateCards', $xml);
    }

    /**
     * Gets usage card by name
     * @return object
     */
    protected function findUsageRatingCardByName($name)
    {
        $data = $this->setData(['name' => $name]);

        $xml = view('elevate-api::xml.usage-rating.find-usage-rating-card-by-name')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findUsageRateCardByName', $xml);
    }

    /**
     * Gets usage card by customer ID
     * @return object
     */
    protected function findUsageRatingCardCustomerID($elevate_customer_id)
    {
        $data = $this->setData(['elevate_customer_id' => $elevate_customer_id]);

        $xml = view('elevate-api::xml.usage-rating.find-usage-rating-card-by-customer-id')->with($data)->render();

        return $this->makeAPICall($this->facade, 'findUsageRateCardToCustomer', $xml);
    }

    /**
     * Creates a usage rating card assignment
     * @param $elevate_customer_id
     * @param $elevate_usage_rating_card_id
     */
    protected function createUsageRatingCardAssignment($elevate_customer_id, $elevate_usage_rating_card_id, $start_date)
    {
        $data = $this->setData(['elevate_customer_id' => $elevate_customer_id, 'elevate_usage_rating_card_id' => $elevate_usage_rating_card_id, 'start_date' => $start_date]);

        $xml = view('elevate-api::xml.usage-rating.create-usage-rating-card-assignment')->with($data)->render();

        return $this->makeAPICall($this->facade, 'createUsageRateCardToCustomerAssignment', $xml);
    }
}