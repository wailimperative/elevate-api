<?php namespace Imperative\ElevateAPI\API\Requests;

abstract class APIRequestAbstract
{
    /**
     * Check to see if request is valid
     * @return bool
     */
    public function isValid() : bool
    {
        foreach ($this->required_fields as $field) {
            if ($this->request_array[$field] == null) return false;
        }

        return true;
    }

    /**
     * get the errors if not valid
     * @return array
     */
    public function getErrors() : array
    {
        $errors = [];

        foreach ($this->required_fields as $field) {
            if ($this->request_array[$field] == null) $errors[] = 'Missing key field: ' . $field;
        }

        return $errors;
    }
}