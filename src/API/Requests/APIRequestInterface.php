<?php namespace Imperative\ElevateAPI\API\Requests;

interface APIRequestInterface
{
    public function getRequestArray(): array;

    public function isValid(): bool;

    public function getErrors() : array;
}