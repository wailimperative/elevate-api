<?php namespace Imperative\ElevateAPI\API\Requests\Customer;

use Carbon\Carbon;
use Imperative\ElevateAPI\API\Requests\APIRequestAbstract;
use Imperative\ElevateAPI\API\Requests\APIRequestInterface;

class CustomerStoreRequest extends APIRequestAbstract implements APIRequestInterface
{
    protected $request_array;

    protected $required_fields = ['bsa', 'start_date', 'payment_terms_in_days', 'name', 'address_line_1', 'town', 'county', 'postcode'];

    public function __construct()
    {
        $this->request_array['bsa'] = null;
        $this->request_array['payment_terms_in_days'] = null;
        $this->request_array['name'] = null;
        $this->request_array['email'] = null;
        $this->request_array['address_line_1'] = null;
        $this->request_array['town'] = null;
        $this->request_array['county'] = null;
        $this->request_array['postcode'] = null;
        $this->request_array['billing_cycle_id'] = null;
        $this->request_array['start_date'] = null;
    }

    /**
     * Sets the required fields
     * @param $bsa
     * @param $name
     * @param $email
     * @param $payment_terms_in_days
     * @param $start_date
     * @return $this
     */
    public function setCustomer(string $bsa, string $name, $email, int $payment_terms_in_days, Carbon $start_date)
    {
        $this->request_array['bsa'] = $bsa;
        $this->request_array['payment_terms_in_days'] = $payment_terms_in_days;
        $this->request_array['name'] = $name;
        $this->request_array['email'] = $email;
        $this->request_array['start_date'] = $start_date->format('Y-m-d');

        return $this;
    }

    /**
     * Sets the address
     * @param $address_line_1
     * @param $town
     * @param $county
     * @param $postcode
     * @return $this
     */
    public function setAddress(string $address_line_1, string $town, string $county, string $postcode)
    {
        $this->request_array['address_line_1'] = $address_line_1;
        $this->request_array['town'] = $town;
        $this->request_array['county'] = $county;
        $this->request_array['postcode'] = $postcode;

        return $this;
    }

    public function setBillingCycleId(int $elevate_billing_cycle_id)
    {
        $this->request_array['billing_cycle_id'] = $elevate_billing_cycle_id;
    }

    /**
     * Uses the set fields and returns the required array layout with the defaults
     * @return array
     */
    public function getRequestArray() : array
    {
        $formatted_array = ['accountNumber' => $this->request_array['bsa'],
            'billingCycleId' => $this->request_array['billing_cycle_id'],
            'customerType' => 'RESIDENTIAL',
            'name' => $this->request_array['name'],
            'startDate' => $this->request_array['start_date'],
            'vip' => 'false',
            'primarySite' => [
                'accountsReference' => $this->request_array['bsa'],
                'siteReference' => 'HOME',
                'startDate' => $this->request_array['start_date'],
                'name' => 'HOME',
                'paymentTermsInDays' => $this->request_array['payment_terms_in_days'],
                'paymentType' => 'CREDIT_CARD',
                'contact' => [
                    'name' => $this->request_array['name'],
                    'contactNameToAppearOnInvoice' => 'true',
                    'role' => 'GENERAL'
                ],
                'siteAddress' => [
                    'addressLine1' => $this->request_array['address_line_1'],
                    'town' => $this->request_array['town'],
                    'county' => $this->request_array['county'],
                    'postCode' => $this->request_array['postcode'],
                ]
            ]
        ];

        if($this->request_array['email']){
            $formatted_array['onlineBillingRequired'] = 'true';
            $formatted_array['emailInviteDispatchDate'] = $this->request_array['start_date'];
            $formatted_array['onlineEmailAddress'] = $this->request_array['email'];

            $formatted_array['primarySite']['contact']['emailAddress'] = $this->request_array['email'];
            $formatted_array['primarySite']['contact']['receiveInvoiceViaEmail'] = 'true';
            $formatted_array['primarySite']['contact']['receiveInvoiceSummaryViaEmail'] = 'true';
            $formatted_array['primarySite']['contact']['receiveElectronicSummaryViaEmail'] = 'true';
        }

        return $formatted_array;
    }
}