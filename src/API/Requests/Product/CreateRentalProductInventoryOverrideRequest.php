<?php namespace Imperative\ElevateAPI\API\Requests\Product;

use Carbon\Carbon;
use Imperative\ElevateAPI\API\Requests\APIRequestAbstract;
use Imperative\ElevateAPI\API\Requests\APIRequestInterface;

class CreateRentalProductInventoryOverrideRequest extends APIRequestAbstract implements APIRequestInterface
{
    protected $request_array;

    protected $required_fields = [
        'start_date',
        'rental_product_inventory_id',
        'price_type',
        'net_price_in_pounds'
        ];

    public function __construct()
    {
        $this->request_array['start_date'] = null;
        $this->request_array['rental_product_inventory_id'] = true;
        $this->request_array['price_type'] = 'ONEOFF';
        $this->request_array['net_price_in_pounds'] = null;
    }

    public function setStartDate(Carbon $start_date)
    {
        $this->request_array['start_date'] = $start_date->format('Y-m-d');

        return $this;
    }

    public function setRentalProductInventoryID(int $rental_product_inventory_id)
    {
        $this->request_array['rental_product_inventory_id'] = $rental_product_inventory_id;

        return $this;
    }

    public function setPriceType(string $price_type)
    {
        $this->request_array['price_type'] = $price_type;

        return $this;
    }

    public function setNetPriceInPounds(float $net_price_in_pounds)
    {
        $this->request_array['net_price_in_pounds'] = $net_price_in_pounds;

        return $this;
    }

    /**
     * Uses the set fields and returns the required array layout with the defaults
     * @return array
     */
    public function getRequestArray() : array
    {
        return [
            'priceInPounds' => $this->request_array['net_price_in_pounds'],
            'priceType' => $this->request_array['price_type'],
            'rentalProductInventoryId' => $this->request_array['rental_product_inventory_id'],
            'startDate' => $this->request_array['start_date'],
            'showOnInvoice' => true,
        ];
    }
}