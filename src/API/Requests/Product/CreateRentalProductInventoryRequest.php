<?php namespace Imperative\ElevateAPI\API\Requests\Product;

use Carbon\Carbon;
use Imperative\ElevateAPI\API\Requests\APIRequestAbstract;
use Imperative\ElevateAPI\API\Requests\APIRequestInterface;

class CreateRentalProductInventoryRequest extends APIRequestAbstract implements APIRequestInterface
{
    protected $request_array;

    protected $required_fields = [
        'aligned_to_billing_cycle',
        'invoice_presentation_name',
        'label',
        'main_billing_number',
        'quantity',
        'elevate_rental_product_id',
        'elevate_site_id',
        'account_number',
        'supplier_id',
        'start_date'
        ];

    public function __construct()
    {
        $this->request_array['aligned_to_billing_cycle'] = null;
        $this->request_array['complete'] = true;
        $this->request_array['invoice_presentation_name'] = null;
        $this->request_array['label'] = null;
        $this->request_array['main_billing_number'] = null;
        $this->request_array['quantity'] = null;
        $this->request_array['elevate_rental_product_id'] = null;
        $this->request_array['elevate_site_id'] = null;
        $this->request_array['account_number'] = null;
        $this->request_array['supplier_id'] = null;
        $this->request_array['start_date'] = null;
        $this->request_array['force_billing'] = true;
    }

    public function setBillingCycle(int $billing_cycle)
    {
        $this->request_array['aligned_to_billing_cycle'] = $billing_cycle;

        return $this;
    }

    public function setInvoicePresentationName(string $presentation_name)
    {
        $this->request_array['invoice_presentation_name'] = $presentation_name;

        return $this;
    }

    public function setLabel(string $label)
    {
        $this->request_array['label'] = $label;

        return $this;
    }

    public function setMainBillingNumber(string $main_billing_number)
    {
        $this->request_array['main_billing_number'] = $main_billing_number;

        return $this;
    }

    public function setQuantity(int $quantity)
    {
        $this->request_array['quantity'] = $quantity;

        return $this;
    }

    public function setRentalProductID(int $elevate_rental_product_id)
    {
        $this->request_array['elevate_rental_product_id'] = $elevate_rental_product_id;

        return $this;
    }

    public function setSiteID(int $elevate_site_id)
    {
        $this->request_array['elevate_site_id'] = $elevate_site_id;

        return $this;
    }

    public function setAccountNumber(string $account_number)
    {
        $this->request_array['account_number'] = $account_number;

        return $this;
    }

    public function setSupplierID(int $supplier_id)
    {
        $this->request_array['supplier_id'] = $supplier_id;

        return $this;
    }

    public function setStartDate(Carbon $start_date)
    {
        $this->request_array['start_date'] = $start_date->format('Y-m-d');

        return $this;
    }

    /**
     * Uses the set fields and returns the required array layout with the defaults
     * @return array
     */
    public function getRequestArray() : array
    {
        return [
            'alignedToBillingCycle' => $this->request_array['aligned_to_billing_cycle'],
            'complete' => $this->request_array['complete'],
            'invoicePresentationName' => $this->request_array['invoice_presentation_name'],
            'label' => $this->request_array['label'],
            'mainBillingNumber' => $this->request_array['main_billing_number'],
            'quantity' => $this->request_array['quantity'],
            'rentalProductId' => $this->request_array['elevate_rental_product_id'],
            'siteId' => $this->request_array['elevate_site_id'],
            'accountNumber' => $this->request_array['account_number'],
            'supplierId' => $this->request_array['supplier_id'],
            'startDate' => $this->request_array['start_date'],
            'forceBilling' => $this->request_array['force_billing']
        ];
    }
}