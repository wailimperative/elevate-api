<?php namespace Imperative\ElevateAPI\API\Requests\Product;

use Carbon\Carbon;
use Imperative\ElevateAPI\API\Requests\APIRequestAbstract;
use Imperative\ElevateAPI\API\Requests\APIRequestInterface;

class CreateUsageProductInventoryRequest extends APIRequestAbstract implements APIRequestInterface
{
    protected $request_array;

    protected $required_fields = [
        'start_date',
        'usage_product_id',
        'elevate_site_id',
        'numbers'
        ];

    public function __construct()
    {
        $this->request_array['start_date'] = null;
        $this->request_array['usage_product_id'] = true;
        $this->request_array['elevate_site_id'] = null;
        $this->request_array['numbers'] = null;
    }

    public function setSiteID(int $elevate_site_id)
    {
        $this->request_array['elevate_site_id'] = $elevate_site_id;

        return $this;
    }

    public function setUsageProduct(int $usage_product_id)
    {
        $this->request_array['usage_product_id'] = $usage_product_id;

        return $this;
    }

    public function setNumbers(string $product_reference, Carbon $start_date, bool $main_billing_number = true)
    {
        $this->request_array['numbers'] = [
            'startDate' => $start_date->format('Y-m-d'),
            'productReference' => $product_reference,
            'mbn' => $main_billing_number
            ];

        return $this;
    }

    public function setStartDate(Carbon $start_date)
    {
        $this->request_array['start_date'] = $start_date->format('Y-m-d');

        return $this;
    }

    /**
     * Uses the set fields and returns the required array layout with the defaults
     * @return array
     */
    public function getRequestArray() : array
    {
        return [
            'startDate' => $this->request_array['start_date'],
            'usageProductId' => $this->request_array['usage_product_id'],
            'siteId' => $this->request_array['elevate_site_id'],
            'numbers' => $this->request_array['numbers']
        ];
    }
}