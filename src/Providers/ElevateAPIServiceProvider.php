<?php namespace Imperative\ElevateAPI\Providers;

use Imperative\ElevateAPI\Console\Commands\Dev\CreateRentalProductInventory;
use Imperative\ElevateAPI\Console\Commands\Dev\CreateRentalRateCardAssignment;
use Imperative\ElevateAPI\Console\Commands\Dev\CreateRentalRateOverride;
use Imperative\ElevateAPI\Console\Commands\Dev\CreateUsageProductInventory;
use Imperative\ElevateAPI\Console\Commands\Dev\CreateUsageRateCardAssignment;
use Imperative\ElevateAPI\Console\Commands\Dev\CustomerCreate;
use Imperative\ElevateAPI\Console\Commands\Dev\FindRentalInventorySummariesByCustomer;
use Imperative\ElevateAPI\Console\Commands\Dev\GetBillingCycles;
use Imperative\ElevateAPI\Console\Commands\Dev\GetCLIByCustomer;
use Imperative\ElevateAPI\Console\Commands\Dev\GetSiteByCustomer;
use Imperative\ElevateAPI\Console\Commands\Dev\GetUsageProducts;
use Imperative\ElevateAPI\Console\Commands\Dev\RentalProductCategories;
use Imperative\ElevateAPI\Console\Commands\Dev\RentalProductsByCategory;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncBillingCycles;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncContractOwners;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncCustomers;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncInventoryRentalProducts;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncInventoryUsageProducts;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncProductCategories;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncRentalProducts;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncRentalProductsByCategory;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncRentalRatingCards;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncSites;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncSuppliers;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncUsageProducts;
use Imperative\ElevateAPI\Console\Commands\Sync\SyncUsageRatingCards;
use Illuminate\Support\ServiceProvider;

class ElevateAPIServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'elevate-api');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
