<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:cus="http://billingcycle.api.billing.imperatives.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
        <cus:findBillingCycles>
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1></arg1>
        </cus:findBillingCycles>
    </soapenv:Body>
</soapenv:Envelope>