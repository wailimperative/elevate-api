<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:cus="http://billingrun.api.billing.imperatives.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
        <cus:findPageOfBillingRuns>
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1></arg1>
            <arg2>
                <firstResult>{!! $first_result !!}</firstResult>
                <maxResults>{!! $max_results !!}</maxResults>
                <maxDownloadResults>{!! $max_download_results !!}</maxDownloadResults>
                <filters></filters>
            </arg2>
        </cus:findPageOfBillingRuns>
    </soapenv:Body>
</soapenv:Envelope>