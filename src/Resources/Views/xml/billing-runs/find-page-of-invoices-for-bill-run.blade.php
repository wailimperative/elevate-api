<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:cus="http://billingrun.api.billing.imperatives.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
        <cus:findPageOfInvoicesForBillRun>
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                <billingRunId>{!! $billing_run_id !!}</billingRunId>
                <filter>
                    <firstResult>0</firstResult>
                    <maxResults>1000</maxResults>
                    <maxDownloadResults>1000</maxDownloadResults>
                </filter>
            </arg1>
        </cus:findPageOfInvoicesForBillRun>
    </soapenv:Body>
</soapenv:Envelope>