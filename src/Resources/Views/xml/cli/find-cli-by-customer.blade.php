<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:cus="http://rentalproductinventory.api.billing.imperatives.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
        <cus:findCLIsByCustomer>
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                <firstResult>{!! $first_result !!}</firstResult>
                <maxResults>{!! $max_results !!}</maxResults>
                <maxDownloadResults>{!! $max_download_results !!}</maxDownloadResults>
                <filters></filters>
            </arg1>
        </cus:findCLIsByCustomer>
    </soapenv:Body>
</soapenv:Envelope>