<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:cus="http://customer.api.billing.imperatives.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
        <cus:createCustomerAndSitesWithDefaults>
            <arg0>
                @include('elevate-api::xml.common.token')
                <contractOwnerId>{!! $contract_owner_id !!}</contractOwnerId>
            </arg0>
            <arg1>
                {!! $xml_data !!}
            </arg1>
        </cus:createCustomerAndSitesWithDefaults>
    </soapenv:Body>
</soapenv:Envelope>