<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ren="http://usage.productinventory.api.billing.imperatives.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
        <ren:createUsageProductInventory>
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                {!! $xml_data !!}
            </arg1>
        </ren:createUsageProductInventory>
    </soapenv:Body>
</soapenv:Envelope>