<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:findAllRentalProductsByCategory xmlns:ns2="http://product.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                {!! $category_id !!}
            </arg1>
        </ns2:findAllRentalProductsByCategory>
    </S:Body>
</S:Envelope>