<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:findAllRentalProducts xmlns:ns2="http://product.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1></arg1>
        </ns2:findAllRentalProducts>
    </S:Body>
</S:Envelope>