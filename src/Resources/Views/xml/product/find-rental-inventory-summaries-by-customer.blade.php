<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ren="http://rental.productinventory.api.billing.imperatives.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
        <ren:findRentalInventorySummariesByCustomer>
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
               {!! $elevate_customer_id !!}
            </arg1>
            <arg3>
                <firstResult>0</firstResult>
                <maxResults>1000</maxResults>
                <maxDownloadResults>1000</maxDownloadResults>
                <filters></filters>
            </arg3>
        </ren:findRentalInventorySummariesByCustomer>
    </soapenv:Body>
</soapenv:Envelope>