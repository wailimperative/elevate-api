<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:ns2="http://rating.api.billing.imperatives.co.uk/">
    <S:Body>
        <ns2:createRentalRateOverrideToInventoryAssignment>
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                {!! $xml_data !!}
            </arg1>
        </ns2:createRentalRateOverrideToInventoryAssignment>
    </S:Body>
</S:Envelope>