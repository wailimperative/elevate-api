<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:createRentalRateCardToCustomerAssignment xmlns:ns2="http://rating.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                <startDate>{!! $start_date !!}</startDate>
                <rentalRateCardId>{!! $elevate_rental_rating_card_id !!}</rentalRateCardId>
                <customerId>{!! $elevate_customer_id !!}</customerId>
            </arg1>
        </ns2:createRentalRateCardToCustomerAssignment>
    </S:Body>
</S:Envelope>
