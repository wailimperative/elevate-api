<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sec="http://security.api.billing.imperatives.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
        <sec:createSecurityToken>
            <arg0>
                <brandName>{!! $brandName !!}</brandName>
                <defaultTokenExpirationInMins>{!! $defaultTokenExpirationInMins !!}</defaultTokenExpirationInMins>
                <secrets>{!! $secrets !!}</secrets>
                <username>{!! $username !!}</username>
            </arg0>
        </sec:createSecurityToken>
    </soapenv:Body>
</soapenv:Envelope>