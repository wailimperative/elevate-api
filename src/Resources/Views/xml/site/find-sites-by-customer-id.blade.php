<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:findSitesByCustomerId xmlns:ns2="http://site.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                {!! $id !!}
            </arg1>
        </ns2:findSitesByCustomerId>
    </S:Body>
</S:Envelope>