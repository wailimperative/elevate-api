<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:findAllSuppliers xmlns:ns2="http://supplier.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1></arg1>
        </ns2:findAllSuppliers>
    </S:Body>
</S:Envelope>