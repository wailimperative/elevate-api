<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:createUsageRateCardToCustomerAssignment xmlns:ns2="http://rating.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                <startDate>{!! $start_date !!}</startDate>
                <usageRateCardId>{!! $elevate_usage_rating_card_id !!}</usageRateCardId>
                <customerId>{!! $elevate_customer_id !!}</customerId>
            </arg1>
        </ns2:createUsageRateCardToCustomerAssignment>
    </S:Body>
</S:Envelope>
