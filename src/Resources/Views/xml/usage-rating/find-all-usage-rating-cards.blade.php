<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:findUsageRateCards xmlns:ns2="http://rating.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                <firstResult>0</firstResult>
                <maxResults>1000</maxResults>
                <maxDownloadResults>1000</maxDownloadResults>
                <filters></filters>
            </arg1>
        </ns2:findUsageRateCards>
    </S:Body>
</S:Envelope>
