<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:findUsageRateCardToCustomer xmlns:ns2="http://rating.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                <findUsageRateCardToCustomerRequest>
                    <id>1</id>
                </findUsageRateCardToCustomerRequest>
            </arg1>
        </ns2:findUsageRateCardToCustomer>
    </S:Body>
</S:Envelope>