<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:findUsageRateCardByName xmlns:ns2="http://rating.api.billing.imperatives.co.uk/">
            <arg0>
                @include('elevate-api::xml.common.token')
            </arg0>
            <arg1>
                Standard
            </arg1>
        </ns2:findUsageRateCardByName>
    </S:Body>
</S:Envelope>